
import sys
import os.path
import json
import calendar
import time

SUCCESSBEGIN = '\033[32m'
ERRBEGIN = '\033[91m'
CEND = '\033[0m'
DESTFILE = 'todos.txt'
STRMINLEN = 5

def file_exists(pFilePath):
	isExist = os.path.exists(pFilePath)
	return isExist

def write_json_db(pDestFile, pDataJson):
	with open(pDestFile, 'w') as outfile:
		json.dump(pDataJson, outfile)

def read_json_db(pSourceFile):
	with open(pSourceFile) as json_file:
		pSourceDb = json.load(json_file)
		return pSourceDb

def get_last_id(pList):
	lastId = 0
	for p in pList:
		lastId = p['id']

	return lastId

def elbora_action(pCurrAct, pArg2, pArg3):

	pDataObj = {}
	pDataObj['todos'] = []
	pCurrId = 0
	ts = calendar.timegm(time.gmtime())
	# semaforo modifica per le opzioni che la richiedono
	dbModificato = False;

	# controllo esistenza file database todos
	pFileCheck = file_exists(DESTFILE)
	# se non aggiungo controllo esistenza del file e quindi dei todos
	if pFileCheck != True and pCurrAct != "a":
			print(ERRBEGIN + "Nessun todo definito. Si pregra di riprovare " + CEND)
			return
	# controllo la lunghezza della stringa in caso di aggiunta o modifica
	if (pCurrAct == "a" and len(pArg2) < STRMINLEN) or (pCurrAct == "e" and len(pArg3) < STRMINLEN):
		print(ERRBEGIN + "Il titolo deve essere lungo almeno " + str(STRMINLEN) + " caratteri " + CEND)
		return

	if pCurrAct == "a":	
		if pFileCheck == True:
			# file esiste invoco il file e conto gli elementi all'interno
			pDataObj = read_json_db(DESTFILE)
			pCurrId = (get_last_id(pDataObj['todos'])+1)

		# accodo il valore al dizionario di liste
		pDataObj['todos'].append({
			'id' : pCurrId,
			'title' : pArg2,
			'done' : "False",
			'timestamp' : ts
		})
		# scrivo il dizionario nel file
		write_json_db(DESTFILE, pDataObj)
	elif pCurrAct == "e":
		# modifica di una voce in base all'id
		# carico il file
		pDataObj = read_json_db(DESTFILE)
		for p in pDataObj['todos']:
			if p['id'] == int(pArg2):
				p['title'] = pArg3
				p['timestamp'] = ts
				dbModificato = True

		# scrivo il dizionation nel file
		write_json_db(DESTFILE, pDataObj)
	elif pCurrAct == "t":
		# toggle del todo se false diventa true altrimenti resta false
		# carico il file
		pDataObj = read_json_db(DESTFILE)
		for p in pDataObj['todos']:
			if p['id'] == int(pArg2):
				p['done'] = "True" if p['done'] == "False" else "False"
				dbModificato = True;

		# scrivo il dizionation nel file
		write_json_db(DESTFILE, pDataObj)	
	elif pCurrAct == "d":
		# cancellazione del todo dal database
		pIndex = 0
		pDel = -1
		# carico il file
		pDataObj = read_json_db(DESTFILE)
		# numero di riferimento per l'indice corrente
		for p in pDataObj['todos']:
			if p['id'] == int(pArg2):
				pDel = pIndex
				dbModificato = True;
				break

			pIndex += 1

		# cancello elemento da lista in base a indice sopra calcolato
		if (pDel > -1):
			del pDataObj['todos'][pDel]

		# scrivo il dizionation nel file
		write_json_db(DESTFILE, pDataObj)
	elif pCurrAct == "s":
		# ricerca todos per titolo
		# lista di appoggio per visualizzare ricerca
		pListaAppoggio = []
		# carico il file
		pDataObj = read_json_db(DESTFILE)
		for p in pDataObj['todos']:
			if (p['title'].find(pArg2) != -1):
				pListaAppoggio.append(p)

		# controllo se la ricerca ha prodotto i risultati sperati
		if len(pListaAppoggio) == 0:
			print(ERRBEGIN + "La ricerca per " + pArg2 
				+" non ha prodotto risultati" + CEND)
		else:
			# lista dei risultati
			pRes = json.dumps(pListaAppoggio)
			print(pRes)

	else:
		# visualizza i todos per data di inserimento decrescente
		# carico il file
		pObj = read_json_db(DESTFILE)
		pSortedObj = {}
		pSortedObj = sorted(pObj['todos'], key=lambda x : x['timestamp'], reverse=True)

		pRes = json.dumps(pSortedObj)
		print(pRes)

	# elif pCurrAct == "s":
	# else:
	# 	# def action ls
	# 	print ("ciao")	

	# se non ho eseguito modifiche avverto l'utente
	if dbModificato == False and pCurrAct != "a" and pCurrAct != "ls" and pCurrAct != "s":
		print(ERRBEGIN + "Nessuna modifica effettuata.\n"
					+ "Todo "+ pArg2 + " non trovato " + CEND)

my_actions = {
				"h" : ">>> mostra tutti le possibili action",
				"ls" : ">>> mostra tutti i todos ordinati per data di inserimento decrescente",
				"a" : "(params: title) >>> aggiunge un todo",
				"d" : "(params: id) >>> cancella un todo",
				"e" : "(params: id, title) >>> edita un todo",
				"t" : "(params: id) >>> fa il toggle del todo",
				"s" : "(params: il termine da cercare) >>> cerca tra i todos e ritorna i todos\n\tcontenenti il termine ricercato nel titolo"
				}


# nome delle script
pScript = sys.argv[0]

# controllo se sono stati passati parametri
if len(sys.argv) == 1:
	print(ERRBEGIN + "Nessun parametro non definito.\n" +
		"Usare " + pScript 
 		+ " h per maggiori dettagli" + CEND)
	exit()

# action dello script letto dagli argomenti
pAction = sys.argv[1].lower()
pParam2 = ""
pParam3 = ""

# controllo esistenza action
if pAction in my_actions:

	# se h visualizzo tutta la lista delle action
	if pAction == "h":
		for k,v in sorted(my_actions.iteritems()):
			print(SUCCESSBEGIN + k + " : " + v + CEND)
	else:
		# controllo input
		if len(sys.argv) < 3 and pAction != "ls" and pAction != "e":
			print(ERRBEGIN + "Secondo parametro non definito.\n" +
						"Utilizzo: \n" + pAction + " : " + my_actions[pAction] +
						"\nSi pregra di riprovare " + CEND)
		elif len(sys.argv) < 4 and pAction == "e":
			print(ERRBEGIN + "Numero di parametri non congruo.\n" +
						"Utilizzo: \n" + pAction + " : " + my_actions[pAction] +
						"\nSi pregra di riprovare " + CEND)
		else:
			pParam2 = sys.argv[1]

			# solo ls necessita di un solo parametro se != h
			if (pAction != "ls"):
				pParam2 = sys.argv[2]

			# se sto editando un todo i parametri sono 3
			if (pAction == "e"):
				pParam3 = sys.argv[3]

			# instrado lo script nelle funzioni di elaborazione
			elbora_action(pAction, pParam2, pParam3)

else:
 	print(ERRBEGIN + pAction + ": argomento non valido oppure non trovato. Usare " + pScript 
 		+ " h per maggiori dettagli" + CEND)

